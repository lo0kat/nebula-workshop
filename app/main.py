from dotenv import dotenv_values
import discord

# Import API_KEY value from .env
config = dotenv_values("../.env")

class MyClient(discord.Client):
    async def on_ready(self):
        print('Logged as {0}'.format(self.user))

    async def on_message(self,message):
        print('Message from {0.author} : {0.content}'.format(message))

client = MyClient()
client.run(config.API_KEY)