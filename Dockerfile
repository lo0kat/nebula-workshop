FROM python:3.10.5-alpine3.16
WORKDIR /opt/code/
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "app/main.py" ]
